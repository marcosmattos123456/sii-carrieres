import React from 'react';
import './App.scss';
import Header from './Components/Header/Header';
import Modal from './Components/Modal/Modal';

const App = (props:any) =>{
  return (<div>
    <Modal />
    <Header />
    {props.children}
  </div>);
}

export default App;
