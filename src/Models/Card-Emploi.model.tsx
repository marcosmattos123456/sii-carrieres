export default interface CardEmploiModel {
    id:number,
    titre:string,
    quantite:number,
    ville:string,
    description?:string,
    candidat:number,
}