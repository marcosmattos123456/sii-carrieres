export interface ModalModel{
    showModal:boolean,
    message:string,
    confirmationAction?: Function,
}