import PubSub from 'pubsub-js';
import {ModalModel} from '../Models/Modal.model';

export function ModalService(newData:ModalModel):void{
    PubSub.publish('modal-data', newData)
}

export function ResetModalService():void{
    PubSub.publish('modal-data', {
        showModal:false,
        message:'',
    })
}