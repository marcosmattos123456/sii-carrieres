import CardEmploiModel from '../Models/Card-Emploi.model';
import PubSub from 'pubsub-js';

const localStorageName = 'poste-liste';

//returne la liste de postes
export default function currentPosteListe():CardEmploiModel[]{
    const data:string = localStorage.getItem(localStorageName) || '';
    return data ? JSON.parse(data) : [];
}

//ajoute un nouveau élément
export function updateItem(item:CardEmploiModel){
    let data = currentPosteListe();
    data.push(item)
    localStorage.setItem(localStorageName, JSON.stringify(data));
    PubSub.publish('poste-liste', data)
}

//supprime un poste
export function deleteItem(id:number){
    let data = localStorage.getItem(localStorageName) || '';
    data = JSON.parse(data).filter( (item:CardEmploiModel) => item.id !== id)
    localStorage.setItem(localStorageName, JSON.stringify(data));
    PubSub.publish('poste-liste', data)
}

//crée la première liste pour essayer
export function createListeToTest(){
    if(localStorage.getItem(localStorageName)) return false;
    localStorage.setItem(localStorageName, JSON.stringify([
        {id:0.1545611, titre: 'Dev Java', quantite: 1, ville: 'Montreal', description:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur id.', candidat: 0},
        {id:0.156189, titre: 'Dev Front-end React', quantite: 5, ville: 'Montreal', description:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur id.', candidat: 1},
        {id:0.84512, titre: 'Dev Front-end Angular', quantite: 2, ville: 'Montreal', description:'Lorem ipsum dolor sit amet, consectetur adipis consectetur adipiscing elit. Curabitur id.', candidat: 0},
        {id:0.41984, titre: 'Dev C#', quantite: 2, ville: 'Montreal', description:'Lorem ipsum dolor sit amet, consectetur adipis consectetur adipiscing elit. Curabitur id.', candidat: 3},
        {id:0.919198, titre: 'Dev React Native', quantite: 1, ville: 'Montreal', description:'Lorem ipsum dolor sit amet, consectetur adipis consectetur adipiscing elit. Curabitur id.', candidat: 0},
    ]))
}