import React from "react";
import './Header.scss';
import {Link} from 'react-router-dom';
import Logo from '../../Assets/img/logo.png';

const Header = () =>{
    return (
        <header className='container header'>
            <Link to='/sii/'>
                <img className='logo' src={Logo} alt='logo'/>
            </Link>
            <h2>Carrières</h2>
        </header>
    )
}

export default Header;