import React, { SyntheticEvent } from "react";
import './CustomInput.scss';

interface Props{
    label: string,
    type: string,
    name:string,
    id:string,
    size?:string,
    value:string|number;
    onChange: Function,
}

const CustomInput = (props:Props) => {

    const onChangeHandler = (e:SyntheticEvent) => props.onChange(e);
    
    return(
            <div className={`custom-input ${props.size ? props.size : ''}`}>
                <label 
                    htmlFor={props.id}>
                        {props.label}
                </label>
                
                <input
                    type={props.type}
                    name={props.name}
                    id={props.id}
                    value={props.value}
                    onChange={onChangeHandler}/>
            </div>
    )
}

export default CustomInput;