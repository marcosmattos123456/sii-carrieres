import React, { SyntheticEvent } from "react";
import './CustomButton.scss';

interface Props{
    text: string,
    onClick: Function,
    extraClass?: string,
} 

const CustomButtom = (props:Props) =>{

    const clickHandler = (e:SyntheticEvent) => props.onClick(e);
    
    return(
        <button className={`custom-button ${props.extraClass ? props.extraClass : ''}` }
                onClick={clickHandler}>
            {props.text}
        </button>
    )
}

export default CustomButtom;