import React from 'react';
import "./CloseButton.scss";

interface Props {
    BtnFunction: Function,
}

const CloseButton = (props:Props) => {

    const clickHandler = () => props.BtnFunction();

    return(
        <div
            className='close-button'
            onClick={clickHandler}>
            x
        </div>
    )
}

export default CloseButton;