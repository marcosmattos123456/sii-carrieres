import React, { useEffect, useState } from 'react';
import './Modal.scss';
import CloseButton from '../CloseButton/CloseButton';
import PubSub from 'pubsub-js';
import { ModalModel } from '../../Models/Modal.model';
import { ResetModalService } from '../../Services/ModalService';
import CustomButtom from '../CustomButton/CustomButton';

const Modal = () =>{

    const firstState:ModalModel = {
        showModal: false,
        message: '',
    }

    const [state, setState] = useState(firstState)

    useEffect(
        () => {
            //component did mount
            PubSub.subscribe('modal-data', (topic:string, data:ModalModel) =>{
                setState(data)
            })
            //component will unmount
            return () => {
                PubSub.unsubscribe('modal-data')
            }
        },
    [])

    const closeModal = () => {   
        ResetModalService();
    }

    return(
        <div>
            {state.showModal ?
                <div className='modal'>
                            <div className='content-wrap'>
                                <CloseButton BtnFunction={closeModal} />
                                <span className='message'>{state.message}</span>

                                {state.confirmationAction ? 
                                    <div className='btn-wrap'>
                                        <CustomButtom text='Annuler' extraClass='notfeatured' onClick={closeModal}/>
                                        <CustomButtom text='Confirmer' onClick={state.confirmationAction}/>
                                    </div>  
                                    :
                                    <button onClick={closeModal} >Ok</button>
                                }

                            </div>
                            <div className='background' onClick={closeModal} ></div>
                </div>
                :
                null
            }
        </div>
    )
}

export default Modal;