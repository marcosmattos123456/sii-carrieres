import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter as Router, Route,Switch} from 'react-router-dom';

import App from './App';
import Home from './Pages/Home/Home';
import New from './Pages/New/New';
import Edit from './Pages/Edit/Edit';

import { createBrowserHistory } from 'history';

export const history = createBrowserHistory({
    basename: process.env.PUBLIC_URL
});

ReactDOM.render((
    <Router>
        <App>
            <Switch>            
                <Route exact path="/sii" component={Home}/>
                <Route path="/sii/new" component={New}/>
                <Route path="/sii/edit/:id" component={Edit}/>
            </Switch>            
        </App>
    </Router>

), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
