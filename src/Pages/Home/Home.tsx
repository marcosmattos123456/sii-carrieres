import React, { useState, useEffect } from 'react';
import CardEmploi from './Card-Emploi/Card-Emploi';
import CardEmploiModel from '../../Models/Card-Emploi.model';
import CustomButtom from '../../Components/CustomButton/CustomButton';
import currentPosteListe, { createListeToTest } from '../../Services/PosteListe.service';

const Home = (props:any) =>{

    const [posteListe, setPosteListe] = useState<CardEmploiModel[]>([])

    const newPoste = () => {
        props.history.push("/sii/new");
    }

    useEffect(
        () =>{
            //ça va créer la première liste pour faire la simualtion de donné
            createListeToTest();
            setPosteListe(currentPosteListe());
            
            PubSub.subscribe('poste-liste', (topic:string, data:CardEmploiModel[]) => {
                setPosteListe(data)
            })

            return () =>{
                PubSub.unsubscribe('poste-liste');
            }
        }, []
    )

    return(
        <div className='container home'>
            {posteListe.map( (poste:CardEmploiModel, index) =>{
                return(
                    <div className='wrap-card' key={index}>
                        <CardEmploi
                            id={poste.id}
                            titre={poste.titre}
                            quantite={poste.quantite}
                            ville={poste.ville}
                            description={poste.description}
                            candidat={poste.candidat} />
                    </div>
                )
            })}

            {posteListe.length < 1 ? <h2>Pas de postes pour l'instant</h2> : ''}

            <CustomButtom text='Créer un nouveau poste' onClick={newPoste}/>

        </div>
    )
}

export default Home;