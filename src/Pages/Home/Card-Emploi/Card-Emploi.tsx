import React, { Component } from "react";
import PubSub from 'pubsub-js';
import './Card-Emploi.scss';
import {Link} from 'react-router-dom';

import CardEmploiModel from '../../../Models/Card-Emploi.model';
import {deleteItem} from '../../../Services/PosteListe.service';
import {ResetModalService} from '../../../Services/ModalService';

import EditIcon from '../../../Assets/img/edit.svg';
import TrashIcon from '../../../Assets/img/trash.svg';
import { ModalModel } from "../../../Models/Modal.model";


export default class CardEmploi extends Component<CardEmploiModel>{

    constructor(Props:CardEmploiModel){
        super(Props);
        this.confirmDelete = this.confirmDelete.bind(this)
    }

    deleteEmploi = (id:number) => {
       deleteItem(id);
       ResetModalService();
    }

    confirmDelete(){
        const modalData:ModalModel= {
            message: `Voulez-vous supprimer le poste ${this.props.titre}?`,
            showModal: true,
            confirmationAction: this.deleteEmploi.bind(this, this.props.id),
        }
        PubSub.publish('modal-data', modalData)
    }

    render(){
        return(
            <div className='card-emploi'>
                <span className='info titre'><strong>{this.props.titre}</strong></span>
                <span className='info quantite'>{this.props.quantite} poste{this.props.quantite > 1 ? 's' : ''}</span>
                <span className='info ville'>{this.props.ville}</span>
                <span className='info description'>{this.props.description}</span>
                <span className={'info candidat ' + (this.props.candidat < 1 ? 'inactive' : '') } >{this.props.candidat} candidat{this.props.candidat > 1 ? 's' : ''}</span>
                <div className='action-wrap'>
                    <Link to='/sii/edit/1'><img className='icon' src={EditIcon} alt='edit icon'/></Link>
                    <img className='icon'  src={TrashIcon} alt='trash icon' onClick={this.confirmDelete} />
                </div>
            </div>
        )
    }
}