import React, { useState, SyntheticEvent } from 'react';
import './New.scss';
import CustomInput from '../../Components/CustomInput/CustomInput';
import CustomButtom from '../../Components/CustomButton/CustomButton';
import CardEmploiModel from '../../Models/Card-Emploi.model';
import {updateItem} from '../../Services/PosteListe.service';


const New = (props:any) =>{

    const [titre, setTitre] = useState('');
    const [quantite, setQuantite] = useState<number>(1);
    const [candidat] = useState(0);
    const [ville, setVille] = useState('');
    const [description, setDescription] = useState('');
    const [errorMsg, setErrorMsg] = useState<string[]>([]);

    function clickBtnAnnuler(){
        props.history.push('/sii/')
    }

    function changeState(event:SyntheticEvent<HTMLInputElement>, setter:Function){
        setter(event.currentTarget.value);
    }

    function checkFields():string[]{
        let errorMsg:string[] = [];
        if(titre === '') errorMsg.push('If faut écrire un titre.');
        if(quantite < 1) errorMsg.push('If faut avoir au moins 1 poste disponible.');
        if(ville === '') errorMsg.push('If faut informer la ville du poste.');
        return errorMsg;
    }

    function sendForm(event:SyntheticEvent) {
        event.preventDefault();
        setErrorMsg([]);

        const posteObj: CardEmploiModel = {
            id: Math.random(),
            candidat,
            quantite,
            titre,
            ville,
            description
        }

        if(checkFields().length > 0) {
            setErrorMsg(checkFields());
            return false;
        }

        updateItem(posteObj);
        props.history.push('/sii/')
    }

    return(
        <div className='container new-poste'>
            <h2>Nouveau poste</h2>

            <form className='new-form'>
                <CustomInput
                    label='Titre du poste*'
                    type='text'
                    name='titre'
                    id='titre'
                    value={titre}
                    onChange={(e:SyntheticEvent<HTMLInputElement>) => changeState(e,setTitre)}
                    />

                <CustomInput
                    label='Quantité de postes*'
                    type='number'
                    name='quantite'
                    id='quantite'
                    size='small'
                    value={quantite}
                    onChange={(e:SyntheticEvent<HTMLInputElement>) => changeState(e,setQuantite)}
                    />

                <CustomInput
                    label='Ville*'
                    type='text'
                    name='ville'
                    id='ville'
                    value={ville}
                    onChange={(e:SyntheticEvent<HTMLInputElement>) => changeState(e,setVille)}
                    />

                <CustomInput
                    label='Description'
                    type='text'
                    name='description'
                    id='description'
                    value={description}
                    onChange={(e:SyntheticEvent<HTMLInputElement>) => changeState(e,setDescription)}
                    />
                
                <div className='error-wrap'>
                    {errorMsg.map( (error:string) =>{
                        return(
                            <span key={error} >{error}</span>
                        )
                    })}
                </div>
                
                <CustomButtom
                    text='Envoyer'
                    onClick={sendForm}
                    />
                
                <CustomButtom
                    text='Annuler'
                    extraClass='notfeatured'
                    onClick={clickBtnAnnuler}
                    />
            </form>

        </div>
    )
}

export default New;